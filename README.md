# AWS Lambda Website Monitor

Generate two environment variables to hold your site name and the port that the site runs on e.g.

```
websiteurl   www.andyhay.info  
websiteport  443  
```

Use the Rackerlabs/lammbda-uploader found here <https://github.com/rackerlabs/lambda-uploader>

Adjust the ``lambda.json`` file to meet your needs.

Use the ``lambda-uploader --no-upload option``, as its the simplest

Change the Configuration->Handler to run against ``WebsiteMonitor.lambda_handler``.

Plug into AWS Lamba function to run every x minutes.

## TODO

* Add Skype4py to push out notifications of when site is down (NOT WORKING YET, CANT GET AROUND ERROR ``Unable to import module 'WebsiteMonitor': cannot import name SkypeAPI``)
* Make notification push out 1 message to say down and one message to say site has returned to activity (may not be possible as it requires state and trying not to use other AWS functions such as a database)