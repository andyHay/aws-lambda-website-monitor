import httplib
import socket
import os
import Skype4Py


def lambda_handler(event, context):
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((os.environ['websiteurl'], int(os.environ['websiteport'])))
    except socket.error, e:
        if 'Connection refused' in e:
            skype_message("Error")
            return "ERROR"
    else:
        if int(os.environ['websiteport']) == 443:
            c = httplib.HTTPSConnection(os.environ['websiteurl'])
        else:
            c = httplib.HTTPConnection(os.environ['websiteurl'])
        c.request("HEAD", '')
        STAT = c.getresponse().status
        if STAT == 200 or STAT == 304:
            skype_message("Alive")
            return "ALIVE"
        else:
            if STAT == 301:
                skype_message("Redirect")
                return "REDIRECT - probably change websiteurl to real endpoint"
            else:
                skype_message("Dead")
                return "DEAD"

def skype_message(message):
    skype = Skype4Py.Skype()
    skype.Attach()
    user = os.environ['skype_user']
    skype.SendMessage(user, message)

